# Willkommen
Dies ist mein Arbeitsprofil, [baumertn](https://gitlab.com/baumertn) ist mein privats Profil.

# Meine Chat Etikette

## Wie ich arbeite
Ich arbeite möglichst im Flow-State, d.h. ich versuche 45 bis 90 Minuten konzentriert zu arbeiten. Nach dieser Phase widme ich mich Chats und E-Mails.

Falls in dieser Zeit sehr viel angefallen ist, kann es natürlich noch länger dauern.

## Was kannst du tun
- Nutze Google. Selbst wenn es nur dafür sorgt, dass du deine Frage konkreter formulieren kannst.
- Falls es um eine Library geht: schau in die jeweiligen Docs. Oft mache ich auch nicht mehr.
- Nutze einen Gruppenchat für generelle Fragen (z.B. auch wenn du eine Lösung per Google gefunden hast, aber unschlüssig bist, ob sie wirklich gut ist).

Sollte das nicht helfen, oder danach noch etwas unklar sein, kannst du weiterhin mich Fragen.

Sollte es sich um etwas handeln, worüber wir schon länger diskutiert haben, bleibe ich natürlich die Anlaufstelle.

## Wie stelle ich eine Frage?
- [Kein „Hallo“](http://nohello.net/de/).
- Code als Text und nicht als Screenshot.
- „X geht nicht“ ist keine Frage. Beschreibe folgendes so detailiert wie möglich:
    1. Was versuchst du zu erreichen? Beachte dabei das [XY-Problem](#das-xy-problem).
    2. Wo bist du (Issue/MR/Branch)?
    3. Wie versuchst du das zu erreichen?
    4. Woran scheitert es? Gibt es eine Fehlermeldung?
    6. Was hast du bisher versucht, um den Fehler zu beheben?

⇒ Das alles sorgt für weniger hin und her.

## Das XY-Problem
- Angenommen du willst `X` tun.
- Du weißt aber nicht, wie du `X` erreichen kannst, aber du _glaubst_, dass dir `Y` dabei hilft  `X` zu erreichen.
- Du weißt auch nicht, wie du `Y` erreichen kannst.
- Du fragst nach einer Lösung für `Y`.
- Dir wird für `Y` geholfen, aber die Helfer finden `Y` ein seltsame Problem.
- Es stellt sich heraus, dass dir `Y` nicht hilft um `X` zu erreichen.
- Fazit: Wir haben viel Zeit verschwendet und `X` ist noch immer nicht gelöst.

Das kannst du dagegen tun:
1. Versuche immer so viel Kontext wie möglich und deine bisherigen Versuche in deiner Frage mitzugeben.
2. Wenn ach mehr Informationen gefragt wird, gib sie.
3. Falls du andere Lösungen bereits ausgeschlossen hast, gib die Gründe an weshalb. Das gibt mehr Kontext zu deinen Anforderungen.
